import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//rxjs
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'
import { PokemonGlobal} from '../../model/pokemon-global.model';
import { PokemonSelected } from '../../model/pokemon-selected.model'


/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
  private baseUrl: string = 'https://pokeapi.co/api/v2/';

  constructor(public http: Http) {

  }

  public getPokemons(urlArg): Promise<PokemonGlobal>{
      var  url = `${this.baseUrl}pokemon/${urlArg}`;
      return this.http.get(url)
      .toPromise()
      .then( response => response.json())
      .catch(error => console.log('Une erreur est survenue' + error ))
  }

  public getPokemon(name): Promise<PokemonSelected>{
    const urlPokemon = `${this.baseUrl}pokemon/${name}`;

    return this.http.get(urlPokemon)
    .toPromise()
    .then(response => response.json())
    .catch(error => console.log('une erreur est survenue' + error ))
  }

}
