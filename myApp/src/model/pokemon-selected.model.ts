import { pokemonSprites } from './pokemon-selected-sprites.model'
import { pokemonTypes } from './pokemon-selected-types.model'

export class PokemonSelected {
  id: number;
  order: number;
  name: string;
  height: number;
  weight: number;
  type: pokemonTypes;
  sprites: pokemonSprites[];

}
