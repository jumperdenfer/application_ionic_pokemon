import { pokemonResult } from './pokemon-result.model'
export class PokemonGlobal {
  count: number;
  next: string;
  previous: boolean;
  results: pokemonResult[];

}
