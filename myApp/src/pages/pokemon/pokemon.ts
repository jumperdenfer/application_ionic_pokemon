import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DataProvider } from '../../providers/data/data';
import { PokemonGlobal} from '../../model/pokemon-global.model';
import { PokemonSelected} from '../../model/pokemon-selected.model';

/**
 * Generated class for the PokemonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-pokemon',
  templateUrl: 'pokemon.html',
})
export class PokemonPage {
  pokes: PokemonGlobal = new PokemonGlobal()
  pokemon: PokemonSelected = new PokemonSelected()

  constructor(public navCtrl: NavController, public navParams: NavParams, private dataPoke: DataProvider) {
    this.navParams.get('navParams')
  }

  ionViewDidLoad() {
    console.log(this.navParams.data['name']);
    this.pokes = this.navParams.data['name'];
  }
  ionViewWillEnter(){
    this.dataPoke.getPokemon(this.pokes).then(newsFetched => {
      this.pokemon = newsFetched;
      console.log(this.pokemon)
    })
  }

}
