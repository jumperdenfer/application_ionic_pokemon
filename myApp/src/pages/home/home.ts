import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { PokemonGlobal} from '../../model/pokemon-global.model';
import { DataProvider } from '../../providers/data/data';
import { PokemonPage } from '../pokemon/pokemon'


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  news: PokemonGlobal = new PokemonGlobal()

  constructor(public navCtrl: NavController, private data: DataProvider,) {

  }
  showPokemon(poke: PokemonGlobal){
    this.navCtrl.push(PokemonPage, poke)
  }
  private offset = 0;
  ionViewDidLoad() {
    this.data.getPokemons('?limit=4')
    .then(newsFetched => {
      this.news = newsFetched;
      console.log(this.news)
    })
  }

   nextButton(){
    var urlBase ='?limit=4&offset='
    var urlArg = urlBase + this.offset
    if(this.offset >= 949){
      this.offset = 945;
      this.data.getPokemons(urlArg)
      .then(newsFetched => {
        this.news = newsFetched;
        console.log(this.news)

      })
    }else{
    this.offset += 4
      this.data.getPokemons(urlArg)
      .then(newsFetched => {
        this.news = newsFetched;
        console.log(this.news)
      })
    }
    console.log(this.offset)
    console.log('urlArg=' + urlArg)
  }

  previousButton(){
    var urlBase ='?limit=4&offset='
    var urlArg = urlBase + this.offset
    if(this.offset <= 0){
      this.offset = 0;
      this.data.getPokemons(urlArg)
      .then(newsFetched => {
        this.news = newsFetched;
        console.log(this.news)
      })
    }else{
      this.offset = this.offset - 4;
      this.data.getPokemons(urlArg)
      .then(newsFetched => {
        this.news = newsFetched;
        console.log(this.news)
      })
    }
    console.log(this.offset)
    console.log('urlArg=' + urlArg)
  }

}
